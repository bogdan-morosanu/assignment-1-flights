<%@ page import="model.Account" %>
<%--
  Created by IntelliJ IDEA.
  User: moro
  Date: 11/8/16
  Time: 6:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Admin Panel</title>
</head>
<body>

<%!

    /**
     * check if accesing visitor is Admin
     */
    public boolean isAdmin(HttpServletRequest r) {

        Cookie[] cookies = r.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals("loginMode") && c.getValue().equals(Account.Type.ADMIN.toString())) {
                    return true;
                }
            }
        }

        return false;
    }

%>

<%
    if (!isAdmin(request)) {
%>
        <jsp:forward page="index.jsp" />
<%
    }
%>

<h1> Welcome, Admin </h1>

<a href="FlightController?action=Insert">Insert New Flight</a>

<table border="1">
    <thead>
    <tr>
        <th>Number</th>
        <th>Type</th>
        <th>Departure City</th>
        <th>Departure Date</th>
        <th>Arrival City</th>
        <th>Arrival Date</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${flightList}" var="f">
        <tr>
            <td><c:out value="${f.number}" /></td>
            <td><c:out value="${f.type}" /></td>
            <td><c:out value="${f.deptCity}" /></td>
            <td><c:out value="${f.deptDate}" /></td>
            <td><c:out value="${f.arrivalCity}" /></td>
            <td><c:out value="${f.arrivalDate}" /></td>
            <td><a href="FlightController?action=Update&number=<c:out value="${f.number}"/>">Update</a></td>
            <td><a href="FlightController?action=Delete&number=<c:out value="${f.number}"/>">Delete</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>

</body>
</html>
