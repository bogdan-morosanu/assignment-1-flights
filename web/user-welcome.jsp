<%@ page import="model.Account" %>
<%@ page import="model.FlightLocalTime" %>
<%@ page import="model.Flight" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: moro
  Date: 11/8/16
  Time: 6:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>User pannel</title>
</head>
<body>


<%!

    /**
     * check if accessing visitor is a User
     */
    public boolean isUser(HttpServletRequest r) {

        Cookie[] cookies = r.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals("loginMode") && c.getValue().equals(Account.Type.USER.toString())) {
                    return true;
                }
            }
        }

        return false;
    }

%>

<%
    if (!isUser(request)) {
%>
<jsp:forward page="index.jsp" />
<%
    }
%>

<h1>
    Welcome, User
</h1>

<form action="FlightController?" method="get">

    <input type="text" name="action" value="UserSelect" readonly="true"> <br/>
    departure city <br/>
    <input type="text" name="deptCity"> <br/>
    <input type="submit"/>
</form>

<br/>
<br/>

<table border="1">
    <thead>
    <tr>
        <th>Number</th>
        <th>Type</th>
        <th>Departure City</th>
        <th>Departure Date</th>
        <th>Arrival City</th>
        <th>Arrival Date</th>
        <th>Local Arrival Date</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${flightList}" var="f">
        <tr>
            <td><c:out value="${f.f.number}" /></td>
            <td><c:out value="${f.f.type}" /></td>
            <td><c:out value="${f.f.deptCity}" /></td>
            <td><c:out value="${f.f.deptDate}" /></td>
            <td><c:out value="${f.f.arrivalCity}" /></td>
            <td><c:out value="${f.f.arrivalDate}" /></td>
            <td><c:out value="${f.localTime}" /></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
