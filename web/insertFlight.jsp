<%@ page import="model.Account" %>
<%--
  Created by IntelliJ IDEA.
  User: moro
  Date: 11/8/16
  Time: 9:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%!

    /**
     * check if accesing visitor is Admin
     */
    public boolean isAdmin(HttpServletRequest r) {

        Cookie[] cookies = r.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals("loginMode") && c.getValue().equals(Account.Type.ADMIN.toString())) {
                    return true;
                }
            }
        }

        return false;
    }

%>

<%
    if (!isAdmin(request)) {
%>
<jsp:forward page="index.jsp" />
<%
    }
%>

<html>
<head>
    <title>Admin Panel</title>
</head>
<body>

<form method="POST" action='FlightController' name="insertFlight">
    number <br/>
    <input type="text" readonly="readonly" name="number" value="<c:out value="${flight.number}" />" /> <br/>
    type <br/>
    <select name="type" selected="<c:out value="${flight.type.toString()}" />" >
        <option value="Airbus A380"> Airbus A380 </option>
        <option value="Boeing 747"> Boeing 747 </option>
    </select>
    <br/>
    departure city <br/>
    <input type="text" name="deptCity" value="<c:out value="${flight.deptCity}" />" /> <br/>
    departure date <br/>
    <input type="text" name="deptDate" value="<c:out value="${flight.deptDate}" />" /> <br/>
    arrival city <br/>
    <input type="text" name="arrivalCity" value="<c:out value="${flight.arrivalCity}" />" /> <br/>
    arrival date <br/>
    <input type="text" name="arrivalDate" value="<c:out value="${flight.arrivalDate}" />" /> <br/>
    <br />
    <input type="submit" value="Submit" />
</form>

</body>
</html>
