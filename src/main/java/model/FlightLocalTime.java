package model;

import db.CityDAO;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Holds both a flight and a local time populated from
 * external request.
 */
public class FlightLocalTime {

    private static final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");

    private Flight f;

    private Date localTime;

    public FlightLocalTime(Flight f) {
        this.f = f;
        this.localTime = localTime(f);
    }

    public FlightLocalTime() { }

    public Flight getF() {
        return f;
    }

    public void setF(Flight f) {
        this.f = f;
    }

    public Date getLocalTime() {
        return localTime;
    }

    public void setLocalTime(Date localTime) {
        this.localTime = localTime;
    }

    public static Date localTime(Flight f) {
        CityDAO cityDao = new CityDAO();
        City from = cityDao.find(f.getDeptCity());
        City to = cityDao.find(f.getArrivalCity());

        int diff = hourDiffGMT(to) - hourDiffGMT(from);

        String dstr = f.getArrivalDate().toString();

        int splitAfter = dstr.indexOf(":");
        int splitBefore = splitAfter - 2;
        String pre = dstr.substring(0, splitBefore);
        String post = dstr.substring(splitAfter);
        String hSrc = dstr.substring(splitBefore, splitAfter);
        String hDst = new Integer(Integer.parseInt(hSrc) + diff).toString();

        try {
            return dateParser.parse(pre + hDst + post);
        } catch(ParseException e) {

            throw new IllegalStateException("malformed date string!");
        }
    }

    public static int hourDiffGMT(City c) {

        String urlString = "http://new.earthtools.org/timezone-1.1/" + c.getLongitude() +"/" + c.getLatitude();

        URL urlObj = null;
        try {
            urlObj = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();

            connection.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));

            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            String result = response.toString();

            int retval = Integer.parseInt(result.substring(response.indexOf("<offset>") + "<offset>".length(),
                                                           response.indexOf("</offset>")));

            System.out.println(result);
            return retval;


        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException("could not parse xml");
        }
    }
}
