package model;

/**
 * Created by moro on 11/7/16.
 */
public class Account {

    public Account () { }

    public Account(String username,
                   String pass,
                   String firstName,
                   String lastName,
                   Type type) {

        this.username = username;
        this.pass = pass;
        this.firstName = firstName;
        this.lastName = lastName;
        this.type = type;

    }

    private String username;

    private String pass;

    private String firstName;

    private String lastName;

    /**
     * encapsulates the types of accounts avaliable
     */
    public enum Type {

        ADMIN, USER;

        public String toString() {

            switch(this) {
                case ADMIN:
                    return "ADMIN";
                case USER:
                    return "USER";

                default:
                    return "UNKNOWN"; // to please IDE
            }
        }
    };

    private Type type;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String toString() {

        return "User: " + this.username +
                " Name: " + this.firstName + " " +this.lastName +
                " Type: " + this.type;

    }

}
