package model;

import java.util.Date;

/**
 * Created by moro on 11/7/16.
 */
public class Flight {

    public Flight() { }

    public Flight(long number,
                  Type type,
                  String deptCity,
                  Date deptDate,
                  String arrivalCity,
                  Date arrivalDate) {

        this.number = number;
        this.type = type;
        this.deptCity = deptCity;
        this.deptDate = deptDate;
        this.arrivalCity = arrivalCity;
        this.arrivalDate = arrivalDate;
    }

    private long number;

    public enum Type {

        A380, B747;

        public String toString() {
            switch(this) {
                case A380:
                    return "Airbus A380";

                case B747:
                    return "Boeing 747";

                default:
                    return "unknown"; // to please ide
            }
        }

        public static Type fromString(String s) {
            if (s.equals("Airbus A380")) {
                return A380;

            } else if (s.equals("Boeing 747")) {
                return B747;

            } else {
                throw new IllegalArgumentException("malformed Flight.Type String!");
            }
        }
    }

    private Type type;

    private String deptCity;

    private Date deptDate;

    private String arrivalCity;

    private Date arrivalDate;

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getDeptCity() {
        return deptCity;
    }

    public void setDeptCity(String deptCity) {
        this.deptCity = deptCity;
    }

    public Date getDeptDate() {
        return deptDate;
    }

    public void setDeptDate(Date deptDate) {
        this.deptDate = deptDate;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }
}
