package db;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by moro on 11/9/16.
 */
public class FlightFilter {

    private Long number;
    private String deptCity;
    private Date deptDate;
    private String arrivalCity;
    private Date arrivalDate;

    static private final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");

    public FlightFilter() { }

    public FlightFilter(String number, String deptCity, String deptDate, String arrivalCity, String arrivalDate) {

        this.number = number != null ? Long.parseLong(number) : null;

        this.deptCity = deptCity;

        try {
            this.deptDate = deptDate != null ? dateParser.parse(deptDate) : null;

        } catch (ParseException e) {
            System.err.println(e);
        }

        this.arrivalCity = arrivalCity;

        try {
            this.arrivalDate = arrivalDate != null ? dateParser.parse(arrivalDate) : null;

        } catch (ParseException e) {
            System.err.println(e);
        }
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getDeptCity() {
        return deptCity;
    }

    public void setDeptCity(String deptCity) {
        this.deptCity = deptCity;
    }

    public Date getDeptDate() {
        return deptDate;
    }

    public void setDeptDate(Date deptDate) {
        this.deptDate = deptDate;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

}
