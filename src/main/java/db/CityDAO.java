package db;

import model.Flight;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import model.City;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Created by moro on 11/7/16.
 */
public class CityDAO {

    public City find(String name) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<City> cities = null;
        City retval = null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE name = :name");
            query.setParameter("name", name);
            cities = query.list();

            if (cities != null && cities.size() == 1) {

                retval = cities.get(0);

            } else {
                // TODO log error
            }

            tx.commit();

        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }

            // TODO log error
            System.err.println(e.toString());

        } finally {
            session.close();
        }

        return retval;
    }

    public void insertCity(String name, double longitude, double latitude) {

        insertCity(new City(name, longitude, latitude));

    }

    public void insertCity(City c) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        session.save(c);

        session.getTransaction().commit();

    }
}
