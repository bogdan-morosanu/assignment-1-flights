package db;

import org.hibernate.Session;
import java.util.*;

import model.Event;


/**
 * Created by moro on 11/7/16.
 */

public class EventDAO {

    public void createAndStoreEvent(String title, Date theDate) {

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Event theEvent = new Event();
        theEvent.setTitle(title);
        theEvent.setDate(theDate);
        session.save(theEvent);

        session.getTransaction().commit();
    }

}