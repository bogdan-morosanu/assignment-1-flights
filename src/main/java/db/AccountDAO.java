package db;

import model.Account;
import model.Event;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Created by moro on 11/7/16.
 */
public class AccountDAO {

    /**
     * returns account from db or null if not found
     */
    public Account findAccount(String username) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<Account> accounts = null;
        Account retval = null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Account WHERE username = :username");
            query.setParameter("username", username);
            accounts = query.list();

            if (accounts != null && accounts.size() == 1) {

                retval = accounts.get(0);

            } else {
                // TODO log error
            }

            tx.commit();

        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }

            // TODO log error
            System.err.println(e.toString());

        } finally {
            session.close();
        }

        return retval;
    }

    public void insertAccount(String username,
                              String pass,
                              String firstName,
                              String lastName,
                              Account.Type type) {

        insertAccount(new Account(username, pass, firstName, lastName, type));

    }

    public void insertAccount(Account acc) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        session.save(acc);

        session.getTransaction().commit();
    }
}
