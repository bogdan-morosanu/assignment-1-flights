package db;

import model.Flight;
import model.FlightLocalTime;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.*;

/**
 * Created by moro on 11/7/16.
 */
public class FlightDAO {

    public List<Flight> listFlights() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<Flight> flights = null;

        try {

            tx = session.beginTransaction();
            flights = session.createQuery("FROM Flight").list();

            tx.commit();

        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }

            // TODO log error

        } finally {
            session.close();

        }

        return flights;
    }

    public List<FlightLocalTime> listFlightsAugmented(FlightFilter flt) {

        String whereClause = "WHERE";

        if (flt.getDeptCity() != null) {
            whereClause += " deptCity = :deptCity ";
        }

        if (flt.getArrivalCity() != null) {
            whereClause += ((whereClause != "WHERE") ? " and " : "") + " arrivalCity = :arrivalCity";
        }

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<Flight> flights = null;

        try {

            tx = session.beginTransaction();
            Query q = session.createQuery("FROM Flight " + whereClause);
            if (flt.getDeptCity() != null) {
                q.setParameter("deptCity", flt.getDeptCity());
            }

            if (flt.getArrivalCity() != null) {
                q.setParameter("arrivalCity", flt.getArrivalCity());
            }

            flights = q.list();
            tx.commit();

        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }

            System.err.println(e);

        } finally {
            session.close();

        }
        List<FlightLocalTime> flightsLocalTime = new ArrayList<FlightLocalTime>();

        for (Flight f : flights) {
            flightsLocalTime.add(new FlightLocalTime(f));
        }

        return flightsLocalTime;

    }

    public void insertFlight(long number,
                             Flight.Type type,
                             String deptCity,
                             Date deptDate,
                             String arrivalCity,
                             Date arrivalDate) {

        insertFlight(new Flight(number, type, deptCity, deptDate, arrivalCity, arrivalDate));

    }

    public void insertFlight(Flight f) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        session.save(f);

        session.getTransaction().commit();
    }

    /**
     * returns flight from db or null if not found
     */
    public Flight findFlight(long number) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        Flight retval = null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE number = :number");
            query.setParameter("number", number);
            flights = query.list();

            if (flights != null && flights.size() == 1) {

                retval = flights.get(0);

            } else {
                // TODO log error
            }

            tx.commit();

        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }

            // TODO log error
            System.err.println(e.toString());

        } finally {
            session.close();
        }

        return retval;
    }

    public void updateFlight(Flight f) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.update(f);
            tx.commit();

        } catch (HibernateException e) {

            if (tx != null) {
                tx.rollback();
            }

            // TODO log error

        } finally {
            session.close();
        }
    }

    public void deleteFlight(Flight f) {
        deleteFlight(f.getNumber());
    }

    public void deleteFlight(long number) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<Flight> flights = null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE number = :number");
            query.setParameter("number", number);
            flights = query.list();

            if (flights.size() != 0) {
                session.delete(flights.get(0));

            } else {
                // TODO log error
            }

            tx.commit();

        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }

            // TODO log error

        } finally {
            session.close();
        }
    }
}
