package servlets;

import db.FlightDAO;
import db.FlightFilter;

import model.Account;
import model.Flight;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.Cookie;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by moro on 11/8/16.
 */
public class FlightController extends HttpServlet {

    private static final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        doFlightUpdate(request, response);

    }

    private void doFlightUpdate(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        FlightDAO dao = new FlightDAO();
        Flight f = dao.findFlight(Long.parseLong(request.getParameter("number")));
        try {
            f.setArrivalDate(dateParser.parse(request.getParameter("arrivalDate")));

        } catch(ParseException e) {
            System.err.println(e.toString());

        };

        f.setArrivalCity(request.getParameter("arrivalCity"));

        try {
            f.setDeptDate(dateParser.parse(request.getParameter("deptDate")));

        } catch(ParseException e) {
            System.err.println(e.toString());
        }

        f.setDeptCity(request.getParameter("deptCity"));

        f.setType(Flight.Type.fromString(request.getParameter("type")));

        dao.updateFlight(f);

        RequestDispatcher disp = request.getRequestDispatcher("/admin-welcome.jsp");
        request.setAttribute("flightList", dao.listFlights());
        disp.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String act = request.getParameter("action");

        if (act.equals("Insert")) {
            // check for security
            if (!checkAdmin(request, response)) {
                response.sendRedirect("/index.jsp");
                return;
            }
            setupFlightUpdate(request, response);

        } else if (act.equals("Update")) {
            // check for security
            if (!checkAdmin(request, response)) {
                response.sendRedirect("/index.jsp");
                return;
            }
            setupFlightUpdate(request, response);

        } else if (act.equals("Delete")) {
            // check for security
            if (!checkAdmin(request, response)) {
                response.sendRedirect("/index.jsp");
                return;
            }
            doFlightDelete(request, response);

        } else if (act.equalsIgnoreCase("UserSelect")) {
            doUserSelect(request, response);
            return; // user logic ends here

        } else {
            response.sendRedirect("/index.jsp");
        }

        // admin only logic
        RequestDispatcher disp = request.getRequestDispatcher("/admin-welcome.jsp");
        FlightDAO dao = new FlightDAO();
        request.setAttribute("flightList", dao.listFlights());
        disp.forward(request, response);
    }

    private void doUserSelect(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        FlightDAO dao = new FlightDAO();
        request.setAttribute("flightList", dao.listFlightsAugmented(
                new FlightFilter(request.getParameter("number"),
                                 request.getParameter("deptCity"),
                                 request.getParameter("deptDate"),
                                 request.getParameter("arrivalCity"),
                                 request.getParameter("arrivalDate"))
        ));


        RequestDispatcher disp = request.getRequestDispatcher("/user-welcome.jsp");
        disp.forward(request, response);
    }

    private void setupFlightUpdate(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        FlightDAO dao = new FlightDAO();
        String str = request.getParameter("number"); // if number is null, this is an insert
        Flight f = (str == null) ? null : dao.findFlight(Long.parseLong(str));

        if (f == null) {
            f = new Flight();
            f.setDeptDate(new Date());
            f.setArrivalDate(new Date());
            dao.insertFlight(f);
        }

        request.setAttribute("flight", f);

        RequestDispatcher disp = request.getRequestDispatcher("/updateFlight.jsp");
        disp.forward(request, response);
    }

    private void doFlightDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        FlightDAO dao = new FlightDAO();
        dao.deleteFlight(Long.parseLong(request.getParameter("number")));
    }

    private boolean checkAdmin(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Cookie[] cs = request.getCookies();
        for (Cookie c : cs) {
            if (c.getName().equals("loginMode") && c.getValue().equals(Account.Type.ADMIN.toString())) {
                return true;
            }
        }

        return false;
    }
}
