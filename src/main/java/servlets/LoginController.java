package servlets;

import db.AccountDAO;
import model.Account;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Created by moro on 11/7/16.
 */
public class LoginController extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
                          throws ServletException, IOException {

        // get request parameters for userID and password
        String user = request.getParameter("username");
        String pwd = request.getParameter("password");

        AccountDAO dao = new AccountDAO();
        Account acc = dao.findAccount(user);

        if (acc != null && acc.getPass().equals(pwd)) {
            Cookie loginCookie = new Cookie("loginMode", acc.getType().toString());

            loginCookie.setMaxAge(3600); // cookie expires in 3600 seconds

            response.addCookie(loginCookie);

            if (acc.getType() == Account.Type.ADMIN) {
                response.sendRedirect("/AccountController?loginMode=" + Account.Type.ADMIN.toString());

            } else {
                response.sendRedirect("/AccountController?loginMode=" + Account.Type.USER.toString());

            }

        } else {

            RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.html");
            PrintWriter out= response.getWriter();
            out.println("<font color=red> User name or password wrong </font>");
            rd.include(request, response);
        }
    }
}
