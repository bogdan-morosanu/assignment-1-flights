package servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import db.FlightDAO;
import model.Account;

/**
 * Created by moro on 11/8/16.
 */
public class AccountController extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException {

        String mode = request.getParameter("loginMode");

        if (mode.equals(Account.Type.ADMIN.toString())) {
            adminWelcome(request, response);


        } else if (mode.equals(Account.Type.USER.toString())){
            userWelcome(request, response);

        } else {
            response.sendRedirect("/login.jsp");
        }
    }

    private void adminWelcome(HttpServletRequest request, HttpServletResponse response)
                              throws ServletException, IOException {

        RequestDispatcher disp = request.getRequestDispatcher("/admin-welcome.jsp");
        FlightDAO dao = new FlightDAO();
        request.setAttribute("flightList", dao.listFlights());
        disp.forward(request, response);
    }

    private void userWelcome(HttpServletRequest request, HttpServletResponse response)
                              throws ServletException, IOException {

        RequestDispatcher disp = request.getRequestDispatcher("/user-welcome.jsp");
        disp.forward(request, response);
    }
}




















