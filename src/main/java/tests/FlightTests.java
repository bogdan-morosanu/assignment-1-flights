package tests;

import db.FlightDAO;
import db.HibernateUtil;
import model.Flight;
import java.util.Date;
import java.util.jar.Pack200;

/**
 * Created by moro on 11/7/16.
 */
public class FlightTests {

    public static void main(String[] args) {

        FlightDAO flightDao = new FlightDAO();

        //flightDao.insertFlight(1, Flight.Type.A380, "Cluj", new Date(), "London", null);

        Date d = new Date();

        final long id = 1019;
        System.out.println("testing insert flight...");
        Flight fInitial = new Flight(id, Flight.Type.B747, "London", d, "Cluj", d);
        flightDao.insertFlight(fInitial);
        System.out.println("Insert Success!");


        System.out.println("testing retrieve flight...");
        Flight f = flightDao.findFlight(fInitial.getNumber());
        if (f != null) {
            System.out.println("Retrieve Success!");

        } else {
            System.err.println("Retrieve Fail!");
            return;
        }

        try {
            System.out.println("sleeping for a bit before update...");
            Thread.sleep(3000);

        } catch(InterruptedException e) {
            // we sleep for test purpose only, no problem here
        }

        System.out.println("testing update...");
        f.setArrivalCity("Frankfurt");

        flightDao.updateFlight(f);
        f = flightDao.findFlight(f.getNumber());

        if (f.getArrivalCity().equals("Frankfurt")) {
            System.out.println("Update Success!");

        } else {
            System.err.println("Update Fail!");
            return;

        }

        System.out.println("testing delete...");
        flightDao.deleteFlight(f.getNumber());
        f = flightDao.findFlight(f.getNumber());

        if (f == null) {
            System.out.println("Delete Success!");
        } else {
            System.err.println("Delete Fail!");
        }

        HibernateUtil.getSessionFactory().close();
    }
}
