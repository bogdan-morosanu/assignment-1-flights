package tests;

import db.AccountDAO;
import db.HibernateUtil;
import model.Account;

/**
 * Created by moro on 11/7/16.
 */
public class AccountTests {

    public static void main(String[] args) {

        AccountDAO accDao = new AccountDAO();
        accDao.insertAccount("dummy-admin", "dps", "A", "B", Account.Type.ADMIN);

        HibernateUtil.getSessionFactory().close();
    }

}
