package tests;

import db.EventDAO;
import db.HibernateUtil;

import java.util.Date;

/**
 * Created by moro on 11/7/16.
 */
public class EventTests {


    public static void main(String[] args) {
        EventDAO mgr = new EventDAO();

        mgr.createAndStoreEvent("Event", new Date());

        HibernateUtil.getSessionFactory().close();
    }
}
